SELECT    cust.firstname ||' '|| cust.lastname CustomerFullName,
SUM(inv.Total)  Total
FROM customers cust LEFT JOIN invoices inv ON cust.customerID = inv.customerID
GROUP BY 1
ORDER BY 2 DESC LIMIT 20;

