/*3 - O componente da questão 3 possui apenas um simples campo de texto.
 Ajuste este componente para que mesmo que a página seja atualizada o valor seja mantido. 
 Se for atualizada a página, o valor deverá ser mantido, porém se for aberta outra aba,
  não deve ser ter o mesmo valor. Desse modo deve ser possível ter duas abas na mesma página,
   e cada uma possuir um valor diferente e mesmo atualizando ambas as páginas os valores de 
   cada uma não serão perdidos.
 */

import { useState } from "react";

function Questao3() {

  const [valor, setValor] = useState("")

  let value;

  if (valor.length > 0) {
    sessionStorage.setItem("storage", valor);
    value = sessionStorage.getItem("storage")
  } else if (valor.length === 0) {
    sessionStorage.setItem("storage2", valor);
    value = sessionStorage.getItem("storage")
  }



  return (
    <div>
      <h1>Questão 3</h1>
      <input value={value} onChange={event => setValor(event.target.value)} />
    </div>
  );
}

export default Questao3; 