import ContextApp from "./MyContext";
import React,{ useState } from "react";

function Comp1() {
    const {texto, setTexto} = React.useContext(ContextApp)
    return (
      <div>
        {texto}
      </div>
    );
  }
  
export default Comp1;
