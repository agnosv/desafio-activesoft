import { useState, useContext } from "react";
import React from "react";
import ContextApp from "./MyContext";

const ProviderApp = ({children}) =>{

    const [texto, setTexto] = useState(' ')


    return(
        <ContextApp.Provider value={{texto, setTexto}}>


        {children}


        </ContextApp.Provider>
        
        );

};


export default ProviderApp;
