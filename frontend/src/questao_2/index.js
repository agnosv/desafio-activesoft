import Comp1 from './comp1'
import Comp2 from './comp2'
import ProviderApp from './MyProvider';

  function Questao2() {
    
    return (
      <ProviderApp>
        <div>
            <h1>Questão 2</h1>
      
             <Comp1 />
             <Comp2 />
        
        </div>
      </ProviderApp>
    );
  }
  
  export default Questao2;
