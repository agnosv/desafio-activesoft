import React,{useState, createContext} from 'react';
import ContextApp from "./MyContext";

function Comp2() {
  const {texto, setTexto} = React.useContext(ContextApp)
   
    return (
      <div>
        <input value={texto} onChange={event => setTexto(event.target.value)} />
      </div>
    );
  }
  
export default Comp2;

 





