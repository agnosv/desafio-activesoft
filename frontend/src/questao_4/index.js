
/*4 -  Conceito Afirnativo )= Nesse componente há uma promise no formato tradicional.*

      a)=Altere a implementação da função `chamarPromise` para a
      nova sintaxe async/await.

      obs: A função `acaoBotao` não deve ter nenhuma linha de código alterada e deverá continuar 
      funcionando como funciona hoje.

       b)= A função `acaoBota2`deverá ser atualizada para utilizar a nova sintaxe de async/await.
    */
function chamarPromise (x){
  let myPromise = new Promise(function(myResolve, myReject) {
    if (x > 7) {
      myResolve('Deu certo')
    } else {
      myReject('Deu errado')
    }
  })
  return myPromise
}

function acaoBotao(){
  chamarPromise(8).then(res => alert(res)).catch(err => alert(err))
}



  async function acaoBotao2(){
    try{
      const promise = await chamarPromise(8);
      alert(promise)
    }
 
    catch(err){
      alert(err);
    }

}


function Questao4() {
    return (
      <div>
        <h1>Questão 4</h1>
        <button onClick={acaoBotao}>Ativar</button>
        <button onClick={acaoBotao2}>Ativar2</button>
      </div>
    );
  }
  
export default Questao4;
