
/*1 - Transforme o componente `NotaField` em um seletor
 de estrelhinhas para dar uma nota.Perceba o funcionamento 
 dos props e mantenha sua API atual, modificando apenas a 
 forma de exibição. */

import React, {useState} from 'react';
import {FaStar} from 'react-icons/fa';

 const NotaField = () => {
  const [rating, setRating] = useState(null);

  return(
    <div>
      {[ ...Array(5)].map((star, i ) => {
        const ratingValue = i + 1;

        return(
          <label>
              <input type="radio"
               name="rating"
               style={{"display":"none"}}
                value={ratingValue}
                 onClick={()=> setRating(ratingValue)}
                  />
              <FaStar color={ratingValue <= rating? "#ffc107" : "#e4e5e9"}size={25}/>
          </label>
         );
      })}
      
    </div>
    
    );
};

export default NotaField; 


